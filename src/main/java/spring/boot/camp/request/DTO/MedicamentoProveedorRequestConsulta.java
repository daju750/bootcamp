package spring.boot.camp.request.DTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor @Builder
public class MedicamentoProveedorRequestConsulta {

	int idmedicamentoproveedor;
	String proveedor;
	String nombre;
	String fecha;
	String precio;
	String cantidad;
}
