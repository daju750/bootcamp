package spring.boot.camp.request.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class MedicamentoProveedorRequestDTO {

	private int idproveedor;
	private String nombre;
	private int precio;
	private int cantidad;
	
}
