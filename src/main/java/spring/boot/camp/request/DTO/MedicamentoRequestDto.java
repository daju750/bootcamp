package spring.boot.camp.request.DTO;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor @Builder
public class MedicamentoRequestDto {

	String nombre;
	Date fechaCreacion;
	boolean estado;
	int precio;
	int cantidad;
	
}
