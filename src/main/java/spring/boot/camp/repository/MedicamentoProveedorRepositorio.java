package spring.boot.camp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.boot.camp.entity.MedicamentoProveedorEntity;

public interface MedicamentoProveedorRepositorio extends JpaRepository<MedicamentoProveedorEntity,Integer>{

}
