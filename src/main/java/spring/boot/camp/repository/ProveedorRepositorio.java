package spring.boot.camp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.boot.camp.entity.ProveedorEntity;

public interface ProveedorRepositorio extends JpaRepository<ProveedorEntity,Integer>{

}
