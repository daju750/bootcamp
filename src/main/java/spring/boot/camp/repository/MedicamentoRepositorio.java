package spring.boot.camp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.boot.camp.entity.MedicamentoEntity;

public interface MedicamentoRepositorio extends JpaRepository<MedicamentoEntity,Integer>{

}
