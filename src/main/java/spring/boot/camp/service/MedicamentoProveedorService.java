package spring.boot.camp.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import spring.boot.camp.request.DTO.MedicamentoProveedorRequestConsulta;
import spring.boot.camp.request.DTO.MedicamentoProveedorRequestDTO;

public interface MedicamentoProveedorService {

	public void Guardar(MedicamentoProveedorRequestDTO medicamentoProveedorDto);
	public List<MedicamentoProveedorRequestConsulta> Consultar(Pageable page);
}
