package spring.boot.camp.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import spring.boot.camp.request.DTO.MedicamentoRequestDto;
import spring.boot.camp.response.DTO.MedicamentoResponseDTO;

public interface MedicamentoService {

	 public List<MedicamentoResponseDTO> GetMedicamentos(Pageable page);
	 public int Guardar(MedicamentoRequestDto medicamentoRequestDto);
	 public void Eliminar(int id);
	
}
