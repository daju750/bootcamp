package spring.boot.camp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

import spring.boot.camp.entity.MedicamentoEntity;
import spring.boot.camp.repository.MedicamentoRepositorio;
import spring.boot.camp.request.DTO.MedicamentoRequestDto;
import spring.boot.camp.response.DTO.MedicamentoResponseDTO;
import spring.boot.camp.service.MedicamentoService;

@Service
public class MedicamentoServiceImpl implements MedicamentoService{

	@Autowired
	private MedicamentoRepositorio medicamentoRepo;
	
	@Override
	public List<MedicamentoResponseDTO> GetMedicamentos(@PageableDefault(page= 0, size= 10)  Pageable page) {
		Page<MedicamentoEntity> medicamentos = this.medicamentoRepo.findAll(page);
		List<MedicamentoResponseDTO> medicamentosDTO = new ArrayList<MedicamentoResponseDTO>();
		
		for(MedicamentoEntity medicamento : medicamentos) {
			MedicamentoResponseDTO medicamentoDTO = new MedicamentoResponseDTO();
			medicamentoDTO.setNombre(medicamento.getNombre());
			medicamentoDTO.setEstado(medicamento.isEstado());
			medicamentoDTO.setFechaCreacion(new Date());
			medicamentosDTO.add(medicamentoDTO);
		}
		return medicamentosDTO;
	}

	@Override
	public int Guardar(MedicamentoRequestDto medicamentoRequestDto) {
		MedicamentoEntity medicamento = new MedicamentoEntity();
		medicamento.setEstado(medicamentoRequestDto.isEstado());
		medicamento.setFechaCreacion(medicamento.getFechaCreacion());
		medicamento.setNombre(medicamentoRequestDto.getNombre());
		medicamento.setCantidad(medicamentoRequestDto.getCantidad());
		medicamento.setPrecio(medicamentoRequestDto.getPrecio());
		return this.medicamentoRepo.save(medicamento).getId();
	}
	
	@Override
	public void Eliminar(int id){
		Optional<MedicamentoEntity> buscarMedicamento = this.medicamentoRepo.findById(id);
		MedicamentoEntity medicamento = new MedicamentoEntity();
		medicamento.setId(buscarMedicamento.get().getId());
		medicamento.setCantidad(buscarMedicamento.get().getCantidad());
		medicamento.setEstado(false);
		medicamento.setNombre(buscarMedicamento.get().getNombre());
		medicamento.setPrecio(buscarMedicamento.get().getPrecio());
		medicamento.setFechaCreacion(buscarMedicamento.get().getFechaCreacion());
		medicamento.setFechaModificacion(buscarMedicamento.get().getFechaModificacion());
		this.medicamentoRepo.save(medicamento);
	}

}
