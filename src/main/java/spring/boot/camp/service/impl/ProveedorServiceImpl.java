package spring.boot.camp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

import spring.boot.camp.entity.ProveedorEntity;
import spring.boot.camp.repository.ProveedorRepositorio;
import spring.boot.camp.response.DTO.ProveedorResponseDTO;
import spring.boot.camp.service.ProveedorService;

@Service
public class ProveedorServiceImpl implements ProveedorService{

	@Autowired
	private ProveedorRepositorio proveedorRepo;
	
	@Override
	public List<ProveedorResponseDTO> GetProveedores(@PageableDefault(page= 0, size= 10)  Pageable page) {

		Page<ProveedorEntity> proveedores = this.proveedorRepo.findAll(page);
		List<ProveedorResponseDTO> medicamentosDTO = new ArrayList<ProveedorResponseDTO>();
		
		for(ProveedorEntity proveedor : proveedores) {
			ProveedorResponseDTO proveedorDTO = new ProveedorResponseDTO();
			proveedorDTO.setIdproveedor(proveedor.getId());
			proveedorDTO.setNombre(proveedor.getNombre());
			proveedorDTO.setEstado(proveedor.isEstado());
			proveedorDTO.setFechaCreacion(new Date());
			medicamentosDTO.add(proveedorDTO);
		}
		
		return medicamentosDTO;
	}
	
	
}