package spring.boot.camp.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;


import spring.boot.camp.entity.MedicamentoProveedorEntity;
import spring.boot.camp.repository.MedicamentoProveedorRepositorio;
import spring.boot.camp.request.DTO.MedicamentoProveedorRequestConsulta;
import spring.boot.camp.request.DTO.MedicamentoProveedorRequestDTO;
import spring.boot.camp.request.DTO.MedicamentoRequestDto;
import spring.boot.camp.service.MedicamentoProveedorService;

@Service
public class MedicamentoProveedorServiceImpl implements MedicamentoProveedorService{

	@Autowired
	private MedicamentoProveedorRepositorio medicamentoProveedorRepo;
	
	@Autowired
	private MedicamentoServiceImpl medicamentoimpl;
	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public void Guardar(MedicamentoProveedorRequestDTO medicamentoProveedorDto) {
		MedicamentoRequestDto medicamentoRequestDto = new MedicamentoRequestDto();
		medicamentoRequestDto.setEstado(true);
		medicamentoRequestDto.setFechaCreacion(new Date());
		medicamentoRequestDto.setNombre(medicamentoProveedorDto.getNombre());
		medicamentoRequestDto.setCantidad(medicamentoProveedorDto.getCantidad());
		medicamentoRequestDto.setPrecio(medicamentoProveedorDto.getPrecio());
		int idMedicamento = this.medicamentoimpl.Guardar(medicamentoRequestDto);		
		MedicamentoProveedorEntity medicamentoProveedor = new MedicamentoProveedorEntity();
		medicamentoProveedor.setIdMedicamento(idMedicamento);
		medicamentoProveedor.setIdProveedor(medicamentoProveedorDto.getIdproveedor());
		
		this.medicamentoProveedorRepo.save(medicamentoProveedor);
	}

	@Override
	@SuppressWarnings("deprecation")
	public List<MedicamentoProveedorRequestConsulta> Consultar(@PageableDefault(page= 0, size= 10)  Pageable page) {
		
		return this.jdbcTemplate.query(
			"select mp.idmedicamentoproveedor,p.nombre as proveedor,m.nombre,m.fechacreacion,m.precio,m.cantidad "
			+ "from medicamentoProveedor mp "
			+ "inner join medicamento m on m.idmedicamento = mp.idmedicamento "
			+ "inner join proveedor p on p.idproveedor = mp.idproveedor where m.estado",
			new Object[]{},
			(rs, rowNum) -> new MedicamentoProveedorRequestConsulta(
			rs.getInt("idmedicamentoproveedor"),
			rs.getString("proveedor"),
			rs.getString("nombre"),
			rs.getString("fechacreacion"),
			rs.getString("precio"),
			rs.getString("cantidad")
			));
		
	}

	
	
}
