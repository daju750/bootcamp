package spring.boot.camp.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import spring.boot.camp.response.DTO.ProveedorResponseDTO;

public interface ProveedorService {

	public List<ProveedorResponseDTO> GetProveedores(Pageable page);
}
