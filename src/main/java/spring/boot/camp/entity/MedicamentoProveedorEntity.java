package spring.boot.camp.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
@Entity @Builder
@Table(name="medicamentoproveedor")
public class MedicamentoProveedorEntity {

	@Id
	@Column(name="idmedicamentoproveedor",unique=true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="idmedicamento")
	private int idMedicamento;
	
	@Column(name="idproveedor")
	private int idProveedor;
	
}
