package spring.boot.camp.entity;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
@Entity @Builder
@Table(name="medicamento")
public class MedicamentoEntity {

	@Id
	@Column(name="idmedicamento",unique=true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="nombre",length = 80)
	private String nombre;
	
	@Column(name="estado")
	private boolean estado;
	
	@Column(name="fechacreacion")
	private Date fechaCreacion;
	
	@Column(name="fechamodificacion")
	private Date fechaModificacion;
	
	@Column(name="precio")
	private int precio;

	@Column(name="cantidad")
	private int cantidad;
	
	public void save() {
	    createdAt();
	}
	
	@PrePersist
	void createdAt() {
	    this.fechaCreacion = new Date();
	}
	
}
