package spring.boot.camp.entity;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
@Entity @Builder
@Table(name="proveedor")
public class ProveedorEntity {

	@Id
	@Column(name="idproveedor",unique=true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="nombre",length = 80)
	private String nombre;
	
	@Column(name="estado")
	private boolean estado;
	
	@Column(name="fechacreacion")
	private Date fechaCreacion;
	
	@Column(name="fechamodificacion")
	private Date fechaModificacion;
	
}
