package spring.boot.camp.Mapper;

import org.springframework.stereotype.Component;

import spring.boot.camp.entity.MedicamentoEntity;
import spring.boot.camp.request.DTO.MedicamentoRequestDto;

@Component
public class MedicamentoMapper {

	public MedicamentoEntity RequestDtoTOEntity(MedicamentoRequestDto medicamentoRequestDto){
		MedicamentoEntity medicamento = new MedicamentoEntity();
		medicamento.setEstado(medicamentoRequestDto.isEstado());
		medicamento.setFechaCreacion(medicamento.getFechaCreacion());
		medicamento.setNombre(medicamentoRequestDto.getNombre());
		return medicamento;
	}
	
}
