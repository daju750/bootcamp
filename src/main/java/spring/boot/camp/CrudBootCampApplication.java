package spring.boot.camp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudBootCampApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudBootCampApplication.class, args);
	}

}
