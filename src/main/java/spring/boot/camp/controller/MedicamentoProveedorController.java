package spring.boot.camp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import spring.boot.camp.form.FormMedicamentoProveedor;
import spring.boot.camp.request.DTO.MedicamentoProveedorRequestDTO;
import spring.boot.camp.service.impl.MedicamentoProveedorServiceImpl;
import spring.boot.camp.service.impl.MedicamentoServiceImpl;
import spring.boot.camp.service.impl.ProveedorServiceImpl;

@Controller
@RequestMapping("/medicamentoProveedor")
public class MedicamentoProveedorController {

	@Autowired
	private ProveedorServiceImpl proveedorimpl;
	
	@Autowired
	private MedicamentoProveedorServiceImpl medicamentoproveedorimpl;
	
	@Autowired
	private MedicamentoServiceImpl medicamentoServiceImpl;
	
	
	@GetMapping
    public ModelAndView getindex(Pageable page){

		ModelAndView modelandview = new ModelAndView("medicamentoProveedor/index");
		modelandview.addObject("GetMedicamentoProveedor",this.medicamentoproveedorimpl.Consultar(page));
    	return modelandview;
    }
	
	
	@GetMapping("/crear")
    public ModelAndView getFormMedicamento(Pageable page){

		ModelAndView modelandview = new ModelAndView("medicamentoProveedor/crear");
		modelandview.addObject("FormMedicamentoProveedor",new FormMedicamentoProveedor());
		modelandview.addObject("GetProveedores",this.proveedorimpl.GetProveedores(page));
		
    	return modelandview;
    }
	
	
	@PostMapping("/crear")
    public ModelAndView postFormMedicamento(FormMedicamentoProveedor formMedicamentoControlador){

		ModelAndView modelandview = new ModelAndView();
		MedicamentoProveedorRequestDTO medicamentoProveedor = new MedicamentoProveedorRequestDTO();
		medicamentoProveedor.setCantidad(formMedicamentoControlador.getCantidad());
		medicamentoProveedor.setIdproveedor(formMedicamentoControlador.getIdproveedor());
		medicamentoProveedor.setNombre(formMedicamentoControlador.getNombre());
		medicamentoProveedor.setPrecio(formMedicamentoControlador.getPrecio());
		this.medicamentoproveedorimpl.Guardar(medicamentoProveedor);
		modelandview.setViewName("redirect:/medicamentoProveedor");
    	return modelandview;
    }

	@GetMapping("/eliminar/{id}")
    public ModelAndView postBorrarMedicamento(@PathVariable("id") int id){

		ModelAndView modelandview = new ModelAndView();
					
		this.medicamentoServiceImpl.Eliminar(id);
		
		modelandview.setViewName("redirect:/medicamentoProveedor");
    	return modelandview;
    }
}