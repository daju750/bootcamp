package spring.boot.camp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping({"/", "/inicio"})
public class Home {

	@GetMapping
    public ModelAndView getindex(){

		ModelAndView modelandview = new ModelAndView("index");
    	return modelandview;
    }
	
}
