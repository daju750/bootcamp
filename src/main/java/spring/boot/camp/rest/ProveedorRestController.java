package spring.boot.camp.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import spring.boot.camp.response.DTO.ProveedorResponseDTO;
import spring.boot.camp.service.impl.ProveedorServiceImpl;

@RestController
@RequestMapping("/proveedor")
public class ProveedorRestController {

	@Autowired
	private ProveedorServiceImpl proveedorimpl;

	@GetMapping()
	@ResponseBody
	public List<ProveedorResponseDTO> GetMedicamentos(@PageableDefault(page= 0, size= 20) Pageable page){
		return this.proveedorimpl.GetProveedores(page);
	}
	
}
