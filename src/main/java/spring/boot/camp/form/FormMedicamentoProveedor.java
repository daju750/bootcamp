package spring.boot.camp.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class FormMedicamentoProveedor {
	
	private int idproveedor;
	private String nombre;
	private int precio;
	private int cantidad;

}
