package spring.boot.camp.response.DTO;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor @Builder
public class ProveedorResponseDTO {

	private int idproveedor;
	private String nombre;
	private boolean estado;
	private Date fechaCreacion;
	private Date fechaModificacion;
	
}
